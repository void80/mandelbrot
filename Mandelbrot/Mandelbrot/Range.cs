﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandelbrot
{
    struct Range<T> where T : IComparable<T>
    {
        private T m_min;
        private T m_max;

        public T Min { get => m_min; }
        public T Max { get => m_max; }
        public T Length { get => (dynamic)Max - (dynamic)Min; }
        public T Center { get => ((dynamic)Max + (dynamic)Min) / 2.0; }

        public Range(T min = default(T), T max = default(T))
        {
            m_min = min;
            m_max = max;
        }

        public override string ToString()
        {
            return string.Format("[{0} - {1}]", Min, Max);
        }

        public static Range<T> operator +(Range<T> range, T offset)
        {
            return new Range<T>((dynamic)range.Min + offset, (dynamic)range.Max + offset);
        }
        public static Range<T> operator -(Range<T> range, T offset)
        {
            return range + (- (dynamic)offset);
        }
    }

    
}
