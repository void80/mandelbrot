﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Mandelbrot
{
    class PixelDrawer
    {
        public PixelDrawer(Grid container)
        {
            m_container = container;

            m_writeableImage = new WriteableBitmap(PixelWidth, PixelHeight, DpiX, DpiY, PixelFormats.Bgra32, null);

            m_bytesPerPixel = (m_writeableImage.Format.BitsPerPixel + 7) / 8;
            m_stride = m_writeableImage.PixelWidth * m_bytesPerPixel;
            m_buffer = new byte[m_writeableImage.PixelHeight * m_stride];
        }

        public void setPixel(int x, int y, Color col)
        {
            int offset = m_stride * y + x * m_bytesPerPixel;
            m_buffer[offset + 0] = col.B;
            m_buffer[offset + 1] = col.G;
            m_buffer[offset + 2] = col.R;
            m_buffer[offset + 3] = col.A;
        }


        public WriteableBitmap draw()
        {
            Int32Rect rect = new Int32Rect(0, 0, m_writeableImage.PixelWidth, m_writeableImage.PixelHeight);
            m_writeableImage.WritePixels(rect, m_buffer, m_stride, 0);
            return m_writeableImage;
            // m_destination.Source = m_writeableImage;
        }

        public int PixelWidth
        {
            get
            {
                return (int)(m_container.ActualWidth * DpiX / WPF_DPI);
            }
        }

        public int PixelHeight
        {
            get
            {
                return (int)(m_container.ActualHeight * DpiY / WPF_DPI);
            }
        }

        public double DpiX
        {
            get
            {
                var source = PresentationSource.FromVisual(m_container);
                return WPF_DPI * source.CompositionTarget.TransformToDevice.M11;
            }
        }

        public double DpiY
        {
            get
            {
                var source = PresentationSource.FromVisual(m_container);
                return WPF_DPI * source.CompositionTarget.TransformToDevice.M22;
            }
        }

        // private Image m_destination;
        private WriteableBitmap m_writeableImage;
        private byte[] m_buffer;
        private int m_stride;
        private int m_bytesPerPixel;
        private Grid m_container;

        private const double WPF_DPI = 96.0;


    }
}
