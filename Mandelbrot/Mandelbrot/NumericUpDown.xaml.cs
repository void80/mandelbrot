﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mandelbrot
{
    /// <summary>
    /// Interaktionslogik für NumericUpDown.xaml
    /// </summary>
    public partial class NumericUpDown : UserControl
    {
        public NumericUpDown()
        {
            InitializeComponent();
            txtNum.Text = m_numValue.ToString();
        }

        public double NumValue
        {
            get { return m_numValue; }
            set
            {
                if(m_numValue != value)
                {
                    m_numValue = value;
                    txtNum.Text = value.ToString();
                    LimitValue();
                }
            }
        }

        public double? MaxValue
        {
            get { return m_maxValue; }

            set
            {
                if(value != m_maxValue)
                {
                    if(value.HasValue)
                    {
                        if(!m_minValue.HasValue || value.Value >= m_minValue.Value)
                        {
                            m_maxValue = value;
                            LimitValue();
                        }
                    }
                    else
                    {
                        m_maxValue = value;
                    }
                }
            }
        }

        public double? MinValue
        {
            get { return m_minValue; }

            set
            {
                if(value != m_minValue)
                {
                    if(value.HasValue)
                    {
                        if(!m_maxValue.HasValue || value.Value <= m_maxValue.Value)
                        {
                            m_minValue = value;
                            LimitValue();
                        }
                    }
                    else
                    {
                        m_minValue = value;
                    }
                }
            }
        }

        public double Delta
        {
            get { return m_delta; }

            set
            {
                if(value != m_delta)
                {
                    m_delta = value;
                }
            }
        }

        private void CmdUp_Click(object sender, RoutedEventArgs e)
        {
            NumValue += Delta;
        }

        private void CmdDown_Click(object sender, RoutedEventArgs e)
        {
            NumValue -= Delta;
        }

        private void TxtNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(txtNum == null)
            {
                return;
            }
            if(double.TryParse(txtNum.Text, out double newValue))
            {
                NumValue = newValue;
            }
            else
            {
                txtNum.Text = m_numValue.ToString();
            }
        }

        private void LimitValue()
        {
            if(m_maxValue.HasValue)
            {
                if(m_numValue > m_maxValue.Value)
                {
                    NumValue = m_maxValue.Value;
                }
            }

            if(m_minValue.HasValue)
            {
                if(m_numValue < m_minValue)
                {
                    NumValue = m_minValue.Value;
                }
            }
        }

        private double m_numValue = 0;
        private double? m_maxValue = null;
        private double? m_minValue = null;
        private double m_delta = 1.0;


    }
}
