﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Numerics;

namespace Mandelbrot
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const double ZOOM_FACTOR = 0.75;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Redraw();
        }

        private void Redraw()
        {
            PixelDrawer drawer = new PixelDrawer(container);
            
            var to = new Range<double>(1, 0);

            double minDimension = Math.Min(drawer.PixelWidth, drawer.PixelHeight);

            var xValue = range.NumValue * drawer.PixelWidth / minDimension;
            var yValue = range.NumValue * drawer.PixelHeight / minDimension;

            var xRange = new Range<double>(-xValue, xValue) - xOffset.NumValue;
            var yRange = new Range<double>(-yValue, yValue) - yOffset.NumValue;

            // actualX.Text = xRange.ToString();
            // actualY.Text = yRange.ToString();

            int[] iterations = new int[drawer.PixelWidth * drawer.PixelHeight];
            int maxIterations = 0;
            int minIterations = m_maxIterations;

            for(int x = 0; x < drawer.PixelWidth; x++)
            {
                for(int y = 0; y < drawer.PixelHeight; y++)
                {
                    Complex original = CreateComplex(drawer, new Point(x, y), xRange, yRange);
                    int currentIterations = CountIterations(original, m_maxIterations);
                    iterations[y * drawer.PixelWidth + x] = currentIterations;
                    if(currentIterations > maxIterations)
                    {
                        maxIterations = currentIterations;
                    }
                    if(currentIterations < minIterations)
                    {
                        minIterations = currentIterations;
                    }
                }
            }

            var from = new Range<int>(minIterations, maxIterations);

            for(int x = 0; x < drawer.PixelWidth; x++)
            {
                for(int y = 0; y < drawer.PixelHeight; y++)
                {
                    byte color = (byte)(Scale(from, to, iterations[y * drawer.PixelWidth + x]) * 255);
                    drawer.setPixel(x, y, Color.FromArgb(255, color, color, color));
                }
            }

            if(maxIterations == m_maxIterations)
            {
                m_maxIterations += (int)Math.Ceiling(m_maxIterations * 0.1);
            }
            

            canvas.Source = drawer.draw();
        }

        private static int CountIterations(Complex original, int maxIterations)
        {
            const double limit = 10e6;
            Complex z = new Complex();
            int result = 0;

            for(result = 0; z.Magnitude < limit && result < maxIterations; result++)
            {
                z = z * z + original;
            }

            return result;
        }

        private Complex CreateComplex(PixelDrawer drawer, Point fromPoint, Range<double> xRange, Range<double> yRange)
        {
            // TODO: only create once (ranges)
            return new Complex
            (
                Scale(new Range<int>(0, drawer.PixelWidth), xRange, fromPoint.X),
                Scale(new Range<int>(0, drawer.PixelHeight), yRange, fromPoint.Y)
            );
        }

        private static double Scale(Range<int> from, Range<double> to, double value)
        {
            return (value - from.Min) * (to.Length / from.Length) + to.Min;
        }

        private void Canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_dragPoint1 = e.GetPosition(sender as UIElement);
            m_dragPoint2 = m_dragPoint1;
            selectionRect.Visibility = System.Windows.Visibility.Visible;
            UpdateRect();
        }

        private void UpdateRect()
        {
            if(m_dragPoint1.HasValue && m_dragPoint2.HasValue)
            {
                var dragPoint1 = m_dragPoint1.Value;
                var dragPoint2 = m_dragPoint2.Value;
                Canvas.SetLeft(selectionRect, Math.Min(dragPoint1.X, dragPoint2.X));
                Canvas.SetTop(selectionRect, Math.Min(dragPoint1.Y, dragPoint2.Y));
                selectionRect.Width = Math.Abs(dragPoint1.X - dragPoint2.X);
                selectionRect.Height = Math.Abs(dragPoint1.Y - dragPoint2.Y);
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed && m_dragPoint2.HasValue)
            {
                m_dragPoint2 = e.GetPosition(sender as UIElement);
                UpdateRect();
            }
        }

        
        private void Canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(m_dragPoint1.HasValue && m_dragPoint2.HasValue)
            {
                AdjustScaling();
                m_dragPoint1 = null;
                m_dragPoint2 = null;
                selectionRect.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void AdjustScaling()
        {
            PixelDrawer drawer = new PixelDrawer(container); // TODO: maybe not use container? Maybe canvas?

            double minDimension = Math.Min(drawer.PixelWidth, drawer.PixelHeight);

            var xValue = range.NumValue * drawer.PixelWidth / minDimension;
            var yValue = range.NumValue * drawer.PixelHeight / minDimension;

            var xRange = new Range<double>(-xValue, xValue) - xOffset.NumValue;
            var yRange = new Range<double>(-yValue, yValue) - yOffset.NumValue;

            Range<int> xPixelRange = new Range<int>(0, drawer.PixelWidth);
            Range<int> yPixelRange = new Range<int>(0, drawer.PixelHeight);

            var newXRange = new Range<double>
            (
                Scale(xPixelRange, xRange, Math.Min(m_dragPoint1.Value.X, m_dragPoint2.Value.X)),
                Scale(xPixelRange, xRange, Math.Max(m_dragPoint1.Value.X, m_dragPoint2.Value.X))
            );
            var newYRange = new Range<double>
            (
                Scale(yPixelRange, yRange, Math.Min(m_dragPoint1.Value.Y, m_dragPoint2.Value.Y)),
                Scale(yPixelRange, yRange, Math.Max(m_dragPoint1.Value.Y, m_dragPoint2.Value.Y))
            );

            var xShrink = newXRange.Length / xRange.Length;
            var yShrink = newYRange.Length / yRange.Length;

            var shrink = Math.Max(xShrink, yShrink);
            range.NumValue *= shrink;

            xOffset.NumValue -= newXRange.Center - xRange.Center;
            yOffset.NumValue -= newYRange.Center - yRange.Center;

            Redraw();

        }

        private Point? m_dragPoint1;
        private Point? m_dragPoint2;
        private int m_maxIterations = 50;

        private void ResetValues(object sender, RoutedEventArgs e)
        {
            range.NumValue = 1.0;
            xOffset.NumValue = 0.0;
            yOffset.NumValue = 0.0;
            Redraw();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ZoomIn();
        }

        private void ZoomIn(double factor = 1.0)
        {
            range.NumValue *= ZOOM_FACTOR * factor;
            Redraw();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ZoomOut();
        }

        private void ZoomOut(double factor = 1.0)
        {
            range.NumValue /= ZOOM_FACTOR * 1.0;
            Redraw();
        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if(e.Delta > 0)
            {
                ZoomIn(e.Delta / 120.0);
            }
            else
            {
                ZoomOut(e.Delta / -120.0);
            }
        }
    }
}
